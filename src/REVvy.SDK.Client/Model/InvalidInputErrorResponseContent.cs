/*
 * ContributeService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = REVvy.SDK.Client.Client.OpenAPIDateConverter;

namespace REVvy.SDK.Client.Model
{
    /// <summary>
    /// InvalidInputErrorResponseContent
    /// </summary>
    [DataContract(Name = "InvalidInputErrorResponseContent")]
    public partial class InvalidInputErrorResponseContent : IEquatable<InvalidInputErrorResponseContent>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidInputErrorResponseContent" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected InvalidInputErrorResponseContent() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="InvalidInputErrorResponseContent" /> class.
        /// </summary>
        /// <param name="code">code (required) (default to 400).</param>
        /// <param name="message">message (required) (default to &quot;Invalid Input&quot;).</param>
        public InvalidInputErrorResponseContent(int code = 400, string message = "Invalid Input")
        {
            this.Code = code;
            // to ensure "message" is required (not null)
            this.Message = message ?? throw new ArgumentNullException("message is a required property for InvalidInputErrorResponseContent and cannot be null");
        }

        /// <summary>
        /// Gets or Sets Code
        /// </summary>
        [DataMember(Name = "code", IsRequired = true, EmitDefaultValue = false)]
        public int Code { get; set; }

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        [DataMember(Name = "message", IsRequired = true, EmitDefaultValue = false)]
        public string Message { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class InvalidInputErrorResponseContent {\n");
            sb.Append("  Code: ").Append(Code).Append("\n");
            sb.Append("  Message: ").Append(Message).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as InvalidInputErrorResponseContent);
        }

        /// <summary>
        /// Returns true if InvalidInputErrorResponseContent instances are equal
        /// </summary>
        /// <param name="input">Instance of InvalidInputErrorResponseContent to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(InvalidInputErrorResponseContent input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Code == input.Code ||
                    this.Code.Equals(input.Code)
                ) && 
                (
                    this.Message == input.Message ||
                    (this.Message != null &&
                    this.Message.Equals(input.Message))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                hashCode = hashCode * 59 + this.Code.GetHashCode();
                if (this.Message != null)
                    hashCode = hashCode * 59 + this.Message.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
