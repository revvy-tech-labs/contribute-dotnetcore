/*
 * ContributeService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = REVvy.SDK.Client.Client.OpenAPIDateConverter;

namespace REVvy.SDK.Client.Model
{
    /// <summary>
    /// Defines ContributorType
    /// </summary>
    
    [JsonConverter(typeof(StringEnumConverter))]
    
    public enum ContributorType
    {
        /// <summary>
        /// Enum INDIVIDUAL for value: INDIVIDUAL
        /// </summary>
        [EnumMember(Value = "INDIVIDUAL")]
        INDIVIDUAL = 1,

        /// <summary>
        /// Enum ORGANIZATION for value: ORGANIZATION
        /// </summary>
        [EnumMember(Value = "ORGANIZATION")]
        ORGANIZATION = 2

    }

}
