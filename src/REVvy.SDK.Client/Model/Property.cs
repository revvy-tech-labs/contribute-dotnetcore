/*
 * ContributeService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.ComponentModel.DataAnnotations;
using OpenAPIDateConverter = REVvy.SDK.Client.Client.OpenAPIDateConverter;

namespace REVvy.SDK.Client.Model
{
    /// <summary>
    /// Property
    /// </summary>
    [DataContract(Name = "Property")]
    public partial class Property : IEquatable<Property>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Property" /> class.
        /// </summary>
        /// <param name="revvyId">revvyId.</param>
        /// <param name="boundary">boundary.</param>
        /// <param name="location">location.</param>
        /// <param name="aliases">aliases.</param>
        public Property(string revvyId = default(string), List<Coordinate> boundary = default(List<Coordinate>), Coordinate location = default(Coordinate), List<Alias> aliases = default(List<Alias>))
        {
            this.RevvyId = revvyId;
            this.Boundary = boundary;
            this.Location = location;
            this.Aliases = aliases;
        }

        /// <summary>
        /// Gets or Sets RevvyId
        /// </summary>
        [DataMember(Name = "revvy_id", EmitDefaultValue = false)]
        public string RevvyId { get; set; }

        /// <summary>
        /// Gets or Sets Boundary
        /// </summary>
        [DataMember(Name = "boundary", EmitDefaultValue = false)]
        public List<Coordinate> Boundary { get; set; }

        /// <summary>
        /// Gets or Sets Location
        /// </summary>
        [DataMember(Name = "location", EmitDefaultValue = false)]
        public Coordinate Location { get; set; }

        /// <summary>
        /// Gets or Sets Aliases
        /// </summary>
        [DataMember(Name = "aliases", EmitDefaultValue = false)]
        public List<Alias> Aliases { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Property {\n");
            sb.Append("  RevvyId: ").Append(RevvyId).Append("\n");
            sb.Append("  Boundary: ").Append(Boundary).Append("\n");
            sb.Append("  Location: ").Append(Location).Append("\n");
            sb.Append("  Aliases: ").Append(Aliases).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Property);
        }

        /// <summary>
        /// Returns true if Property instances are equal
        /// </summary>
        /// <param name="input">Instance of Property to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Property input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.RevvyId == input.RevvyId ||
                    (this.RevvyId != null &&
                    this.RevvyId.Equals(input.RevvyId))
                ) && 
                (
                    this.Boundary == input.Boundary ||
                    this.Boundary != null &&
                    input.Boundary != null &&
                    this.Boundary.SequenceEqual(input.Boundary)
                ) && 
                (
                    this.Location == input.Location ||
                    (this.Location != null &&
                    this.Location.Equals(input.Location))
                ) && 
                (
                    this.Aliases == input.Aliases ||
                    this.Aliases != null &&
                    input.Aliases != null &&
                    this.Aliases.SequenceEqual(input.Aliases)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.RevvyId != null)
                    hashCode = hashCode * 59 + this.RevvyId.GetHashCode();
                if (this.Boundary != null)
                    hashCode = hashCode * 59 + this.Boundary.GetHashCode();
                if (this.Location != null)
                    hashCode = hashCode * 59 + this.Location.GetHashCode();
                if (this.Aliases != null)
                    hashCode = hashCode * 59 + this.Aliases.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
