using System;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// Contributor
    /// </summary>
    public class Contributor
    {

        /// <summary>
        /// Initializes a new instance of the Contributor class.
        /// </summary>
        /// <param name="id">id (required).</param>
        public Contributor(string id = default)
        {
            // to ensure "id" is required (not null)
            Id = id ?? throw new ArgumentNullException("id is a required property for Contributor and cannot be null");
        }

        /// <summary>
        /// Initializes a new instance of the Contributor class.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name.</param>
        /// <param name="type">type.</param>
        /// <param name="reputation">reputation.</param>
        public Contributor(string id, string name, ContributorType type, int reputation) : this(id)
        {
            Id = id;
            Name = name;
            Type = type;
            Reputation = reputation;
        }

        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Type 
        /// </summary>
        public ContributorType Type { get; set; }

        /// <summary>
        /// Gets or Sets OrganizationId
        /// </summary>
        public string Organizationid { get; set; }

        /// <summary>
        /// Gets or Sets Reputation 
        /// </summary>
        public int Reputation { get; set; }
    }
}
