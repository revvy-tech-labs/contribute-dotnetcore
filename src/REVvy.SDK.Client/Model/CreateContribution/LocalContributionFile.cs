using System;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// LocalContributionFile
    /// </summary>
    public partial class LocalContributionFile
    {
        /// <summary>
        /// Initializes a new instance of the LocalContributionFile class.
        /// </summary>
        public LocalContributionFile() { }

        /// <summary>
        /// Initializes a new instance of the LocalContributionFile class.
        /// </summary>
        /// <param name="filePath">filePath (required).</param>
        public LocalContributionFile(string filePath = default)
        {
            // to ensure "id" is required (not null)
            FilePath = filePath ?? throw new ArgumentNullException("filePath is a required property for LocalContributionFile and cannot be null");
        }

        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        public string FilePath { get; set; }
    }
}
