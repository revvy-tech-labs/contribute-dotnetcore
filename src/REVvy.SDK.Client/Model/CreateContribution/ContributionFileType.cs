using System;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// ContributionFileType
    /// </summary>
    public class ContributionFileType
    {
        /// <summary>
        /// Initializes a new instance of the ContributionFileType class.
        /// </summary>
        public ContributionFileType()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ContributionFileType class.
        /// </summary>
        /// <param name="localFile">localFile (required).</param>
        public ContributionFileType(LocalContributionFile localFile = default(LocalContributionFile))
        {
            // to ensure "localFile" is required (not null)
            this.LocalFile = localFile ?? throw new ArgumentNullException("localFile is a required property for ContributionFileType and cannot be null"); ;
        }

        /// <summary>
        /// Initializes a new instance of the ContributionFileType class.
        /// </summary>
        /// <param name="s3File">localFile (required).</param>
        public ContributionFileType(S3ContributionFile s3File = default(S3ContributionFile))
        {
            // to ensure "localFile" is required (not null)
            this.s3File = s3File ?? throw new ArgumentNullException("localFile is a required property for ContributionFileType and cannot be null"); ;
        }

        /// <summary>
        /// Gets or Sets LocalFile
        /// </summary>
        public LocalContributionFile LocalFile { get; set; }

        /// <summary>
        /// Gets or Sets s3File
        /// </summary>
        public S3ContributionFile s3File { get; set; }
    }
}
