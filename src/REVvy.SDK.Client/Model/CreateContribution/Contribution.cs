using System;
using System.Collections.Generic;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// Contribution
    /// </summary>
    public class Contribution
    {
        /// <summary>
        /// Initializes a new instance of the contribution class.
        /// </summary>
        /// <param name="documents">documents (required).</param>
        /// <param name="type">type (required).</param>
        /// <param name="property">property (required).</param>
        /// <param name="comments">comments (required).</param>
        /// <param name="roles">roles (required).</param>
        /// <param name="referenceId">referenceId (required).</param>
        public Contribution(ContributionType type = default(ContributionType), List<RevvyDocument> documents = default, Property property = default, string comments = default, List<Role> roles = default, string referenceId = default)
        {
            // to ensure "document" is required (not null)
            Documents = documents ?? throw new ArgumentNullException("documents is a required property for Contribution and cannot be null");
            // to ensure "property" is required (not null)
            Property = property ?? throw new ArgumentNullException("property is a required property for Contribution and cannot be null");
            // to ensure "type" is required (not null)
            Type = Enum.IsDefined(typeof(ContributionType), type) ? type : throw new ArgumentNullException("type is a required property for Contribution and cannot be null");
            Comments = comments;
            Roles = roles;
            ReferenceId = referenceId;
        }

        /// <summary>
        /// Initializes a new instance of the contribution class.
        /// </summary>
        public Contribution()
        {
        }

        /// <summary>
        /// Gets or Sets Type 
        /// </summary>
        public ContributionType Type { get; set; }

        /// <summary>
        /// Gets or Sets Documents
        /// </summary>
        public List<RevvyDocument> Documents { get; set; }

        /// <summary>
        /// Gets or Sets Property
        /// </summary>
        public Property Property { get; set; }

        /// <summary>
        /// Gets or Sets PropertyId
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or Sets Roles
        /// </summary>
        public List<Role> Roles { get; set; }

        /// <summary>
        /// Gets or Sets ReferenceId
        /// </summary>
        public string ReferenceId { get; set; }
    }
}
