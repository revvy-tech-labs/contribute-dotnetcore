using System;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// RevvyDocument
    /// </summary>
    public partial class RevvyDocument
    {
        /// <summary>
        /// Initializes a new instance of the RevvyDocument class.
        /// </summary>
        public RevvyDocument() { }

        /// <summary>
        /// Initializes a new instance of the RevvyDocument class.
        /// </summary>
        /// <param name="file">fileName (required).</param>
        /// <param name="type">type (required).</param>
        /// <param name="date">date (required).</param>
        /// <param name="subtype">subtype (required).</param>
        /// <param name="comments">comments.</param>
        /// <param name="referenceId">referenceid.</param>
        public RevvyDocument(ContributionFileType file = default, string date = default, DocType type = default, string comments = default, string subtype = default, string referenceId = default)
        {
            // to ensure "file" is required (not null)
            File = file ?? throw new ArgumentNullException("file is a required property for RevvyCDocument and cannot be null"); ;
            // to ensure "file" is required (not null)
            Date = date ?? throw new ArgumentNullException("date is a required property for RevvyCDocument and cannot be null"); ;
            // to ensure "type" is required (not null)
            Type = Enum.IsDefined(typeof(DocType), type) ? type : throw new ArgumentNullException("type is a required property for RevvyCDocument and cannot be null");
            Subtype = subtype;
            Comments = comments;
            ReferenceId = referenceId;
        }

        /// <summary>
        /// Gets or Sets File
        /// </summary>
        public ContributionFileType File { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Gets or Sets Type
        /// </summary>
        public DocType Type { get; set; }

        /// <summary>
        /// Gets or Sets Subtype
        /// </summary>
        public string Subtype { get; set; }

        /// <summary>
        /// Gets or Sets ReferenceId
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// Gets or Sets Comments
        /// </summary>
        public string Comments { get; set; }
    }
}
