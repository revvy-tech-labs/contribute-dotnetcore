﻿using System;
using System.Collections.Generic;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// CreateContributionRequest
    /// </summary>
    public partial class CreateContributionRequest
    {
        /// <summary>
        /// Initializes a new instance of the CreateContributionRequest class.
        /// </summary>
        public CreateContributionRequest() { }

        /// <summary>
        /// Initializes a new instance of the CreateContributionRequest class.
        /// </summary>
        /// <param name="contributions">contributions (required).</param>
        public CreateContributionRequest(List<Contribution> contributions)
        {
            // to ensure "contributions" is required (not null)
            Contributions = contributions ?? throw new ArgumentNullException("contributions is a required property for CreateContributionRequest and cannot be null");
            // to ensure "referenceId" is required (not null)
        }

        /// <summary>
        /// Gets or Sets Contributions
        /// </summary>
        public List<Contribution> Contributions { get; set; }
    }
}
