using System;
using System.Collections.Generic;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// Role
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public Role() { }

        /// <summary>
        /// Initializes a new instance of the class with values.
        /// </summary>

        public Role(RoleType role, List<Contributor> contributors)
        {
            // to ensure "role" is required (not null)
            RoleType = Enum.IsDefined(typeof(RoleType), role) ? role : throw new ArgumentNullException("role is a required property for CRole and cannot be null");
            Contributors = contributors;
        }

        /// <summary>
        /// Gets or Sets RoleType
        /// </summary>
        public RoleType RoleType { get; set; }

        /// <summary>
        /// Gets or Sets Contributors
        /// </summary>
        public List<Contributor> Contributors { get; set; }
    }
}
