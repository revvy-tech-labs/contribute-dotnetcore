using System;

namespace REVvy.SDK.Client.Model.CreateContribution
{
    /// <summary>
    /// S3ContributionFile
    /// </summary>
    public class S3ContributionFile
    {
        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        public S3ContributionFile() { }

        /// <summary>
        /// Initializes a new instance of the class.
        /// </summary>
        /// <param name="bucketName">bucketName (required).</param>
        /// <param name="key">key (required).</param>
        /// <param name="awsKeyId">awsKeyId (required).</param>
        /// <param name="awsSecret">awsSecret (required).</param>
        public S3ContributionFile(string bucketName, string key, string awsKeyId, string awsSecret)
        {
            // to ensure "bucketName" is required (not null)
            BucketName = bucketName ?? throw new ArgumentNullException("bucketName is a required property for S3ContributionFile and cannot be null");
            // to ensure "key" is required (not null)
            Key = key ?? throw new ArgumentNullException("key is a required property for S3ContributionFile and cannot be null");
            // to ensure "awsKeyId" is required (not null)
            AWSKeyId = awsKeyId ?? throw new ArgumentNullException("awsKeyId is a required property for S3ContributionFile and cannot be null");
            // to ensure "awsSecret" is required (not null)
            AWSSecret = awsSecret ?? throw new ArgumentNullException("awsSecret is a required property for S3ContributionFile and cannot be null");
        }

        /// <summary>
        /// Gets or Sets BucketName
        /// </summary>
        public string BucketName { get; set; }

        /// <summary>
        /// Gets or Sets Key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or Sets AWSKeyId
        /// </summary>
        public string AWSKeyId { get; set; }

        /// <summary>
        /// Gets or Sets AWSSecret
        /// </summary>
        public string AWSSecret { get; set; }
    }
}
