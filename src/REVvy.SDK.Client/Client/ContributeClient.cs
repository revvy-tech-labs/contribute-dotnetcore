﻿using Amazon;
using Amazon.Runtime.Internal.Endpoints.StandardLibrary;
using Amazon.S3;
using Amazon.S3.Model;
using DZen.Security.Cryptography;
using Newtonsoft.Json.Linq;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.ENUM;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Model.CreateContribution;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography;
using System.Text;

namespace REVvy.SDK.Contribute.Client
{
    /// <summary>
    /// 
    /// </summary>
    public class ContributeClient
    {
        private DefaultApi API = new DefaultApi();
        private string xApiKey = null;
        private string xRevvyKeyID = null;
        private string uploadSessionID = null;

        /// <summary>
        /// 
        /// </summary>
        public string RevvyKeyID   // property
        {
            get { return xRevvyKeyID; }
            set { xRevvyKeyID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string APIKey   // property
        {
            get { return xApiKey; }
            set { xApiKey = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string UploadSessionID   // property
        {
            get { return uploadSessionID; }
            set { uploadSessionID = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xApiKey"></param>
        /// <param name="xRevvyKeyID"></param>
        public ContributeClient(string xRevvyKeyID, string xApiKey)
        {
            this.xRevvyKeyID = xRevvyKeyID;
            this.xApiKey = xApiKey;
        }

        /// <summary>
        /// Method used to create an contribution
        /// </summary>
        /// <param name="createContributionRequest">Object with the information to be
        /// uploaded</param> <returns>Upload session id</returns>
        public string CreateContribution(
            CreateContributionRequest createContributionRequest)
        {
            string tempFolder = GetTemporaryDirectory();
            string tempFolderFiles = Path.Combine(tempFolder, "files");
            int countFileToUpload = 0;

            Directory.CreateDirectory(tempFolderFiles);

            List<ContributionInput> contributions = new List<ContributionInput>();

            foreach (var contribution in createContributionRequest.Contributions)
            {
                List<DocumentInputObj> documents = new List<DocumentInputObj>();

                foreach (var document in contribution.Documents)
                {
                    try
                    {
                        FileInfo file =
                            GetAndSaveContributionFile(document.File, tempFolder);

                        if (file == null)
                        {
                            continue;
                        }

                        string fileSha256 = getSHA256(file.FullName);

                        documents.Add(new DocumentInputObj(
                            referenceId: document.ReferenceId ?? "",
                            date: document.Date,
                            sha3256: fileSha256,
                            type: document.Type,
                            subtype: document.Subtype ?? "",
                            fileSize: (int)file.Length,
                            fileName: file.Name));

                        SaveFileIntoDirectory(file, tempFolderFiles);

                        countFileToUpload++;
                    }
                    catch (Exception e)
                    {
                        Console.Out.WriteLine(
                            $"--- Error reading the file {e.ToString()}");
                        continue;
                    }
                }

                contributions.Add(new ContributionInput(
                    type: contribution.Type,
                    referenceId: contribution.ReferenceId ?? "",
                    property: contribution.Property, documents: documents,
                    roles: MapAndBuildRoleList(contribution.Roles),
                    comments: contribution.Comments));
            }

            string newZipPath = $"{tempFolderFiles}.zip";
            ZipFile.CreateFromDirectory(tempFolderFiles, newZipPath);

            string zipSHA256 = getSHA256(newZipPath);
            int zipSize = (int)new FileInfo(newZipPath).Length;

            Console.Out.WriteLine($"--- Zip created with {countFileToUpload} files");

            CreateUploadSessionRequestContent createRequest =
                new CreateUploadSessionRequestContent(fileSize: zipSize,
                                                      sha3256: zipSHA256,
                                                      contributions: contributions);

            Console.Out.WriteLine($"--- Contribution request created");

            CreateUploadSession(createRequest);

            Console.Out.WriteLine($"--- Uploading {countFileToUpload} files");

            UploadFile(newZipPath);

            GetUploadSessionResponseContent uploadSessionID = GetUploadSession();

            Console.Out.WriteLine($"--- New session id: {uploadSessionID}");

            //var partsInfo = GetUploadListParts();
            //Console.Out.WriteLine("--- Session date expires: " +
            //                      partsInfo.SessionExpiresAt);
            //Console.Out.WriteLine("--- Total parts number: " + partsInfo.TotalParts);
            //Console.Out.WriteLine("--- Total processed parts number: " +
            //                      partsInfo.NumPartsProcessed);

            //foreach (var part in partsInfo.Parts)
            //{
            //    Console.Out.WriteLine("--- Part Number: " + part.PartNumber);
            //    Console.Out.WriteLine("--- Part ID: " + part.ETag);
            //    Console.Out.WriteLine("--- Part Size: " + part.Size);
            //}

            CommitUploadSession(this.uploadSessionID);
            Console.Out.WriteLine("--- Committed");

            RemoveDirectory(tempFolder);

            return this.uploadSessionID;
        }
        /// <summary>
        /// Method used to create the upload session ID, the sessionID will be tied to this object
        /// </summary>
        /// <param name="createUploadSessionRequestContent">Object with the information to be uploaded</param>
        /// <returns>Created session</returns>
        public CreateUploadSessionResponseContent CreateUploadSession(CreateUploadSessionRequestContent createUploadSessionRequestContent)
        {
            CreateUploadSessionResponseContent result = API.CreateUploadSession(this.xRevvyKeyID, this.xApiKey, createUploadSessionRequestContent);
            this.UploadSessionID = result.Data.Session.UploadSessionId;
            return result;
        }

        /// <summary>
        /// Method used to find contributors
        /// </summary>
        /// <param name="id">Contributor id</param>
        /// <param name="name">Contributor name</param>
        /// <param name="type">Contributor type</param>
        /// <returns>List of contributors</returns>
        public List<Contributor> FindContributor(string id = null, string name = null, ContributorType type =
        default(ContributorType))
        {
            // CreateUploadSessionResponseContent result = API.CreateUploadSession(this.xRevvyKeyID, this.xApiKey, createUploadSessionRequestContent);
            // this.UploadSessionID = result.Data.UploadSessionId;
            List<Contributor> contributors = new List<Contributor>();
            string query = "";

            if (id != null)
            {
                query = id;
            }

            if (name != null)
            {
                query = name;
            }

            GetContributorsResponseContent response = API.GetContributors(this.xRevvyKeyID, this.xApiKey, type: type, q: query);

            response.Data.Contributors.ForEach((item) =>
            {
                contributors.Add(new Contributor(
                      id: item.Id,
                      name: item.Name,
                      type: item.Type,
                      reputation: item.Reputation));
            });
            return contributors;
        }

        /// <summary>
        /// Method used to find contributions
        /// </summary>
        /// <param name="id">Contribution id</param>
        /// <param name="referenceId">Contribution referenceId</param>
        /// <returns>List of contributions</returns>
        public List<ContributeDetailRef> FindContribution(string id = null, string referenceId = null)
        {
            if (id == null && referenceId == null)
            {
                throw new ArgumentNullException("Id or referenceId is required");
            }

            GetUploadSessionRefResponseContent response = API.GetUploadSessionRef(this.xApiKey, contributionId: id, referenceId: referenceId);

            return response.Data.Contributions;
        }

        /// <summary>
        /// Method used to delete document
        /// </summary>
        /// <param name="contributionId">Contribution id</param>
        /// <param name="documentId">Document id</param>
        /// <returns>Delete message</returns>
        public string DeleteDocument(string contributionId, string documentId)
        {
            if (contributionId == null)
            {
                throw new ArgumentNullException("ContributionId is required");
            }

            if (documentId == null)
            {
                throw new ArgumentNullException("DocumentId is required");
            }

            DeleteDocumentResponseContent response = API.DeleteDocument(contributionId, documentId, this.xRevvyKeyID, this.xApiKey);

            return response.Msg;
        }

        /// <summary>
        /// Method used to get the file from AWS S3
        /// </summary>
        /// <param name="bucketName">S3 bucketName</param>
        /// <param name="key">S3 key</param>
        /// <param name="awsKeyId">S3 AWSKeyId</param>
        /// <param name="awsSecret">S3 AWSSecret</param>
        /// <param name="region">S3 Region</param>
        /// <param name="storeFolderPath">S3 StoreFolderPath</param>
        /// <returns>S3 file</returns>
        public static FileInfo GetS3File(string bucketName, string key, string awsKeyId,
                                         string awsSecret, string storeFolderPath,
                                         RegionEndpoint region = null)
        {
            var s3Config = new AmazonS3Config
            {
                RegionEndpoint = region ?? RegionEndpoint.USEast1,
            };

            using (var s3Client = new AmazonS3Client(awsKeyId, awsSecret, s3Config))
            {
                var request =
                    new GetObjectRequest { BucketName = bucketName, Key = key };

                using (var response = s3Client.GetObjectAsync(request).Result)
                {
                    string uniqueId = Guid.NewGuid().ToString();
                    string fileName =
                        $"{Path.GetFileNameWithoutExtension(key)}_{uniqueId}{Path.GetExtension(key)}";
                    string filePath = Path.Combine(storeFolderPath, fileName);

                    using (var fileStream = File.Create(filePath))
                    {
                        response.ResponseStream.CopyTo(fileStream);
                    }

                    Console.Out.WriteLine(
                        $"--- File downloaded successfully: {bucketName}/{key}, as {fileName}");

                    return new FileInfo(filePath);
                }
            }
        }
        /// <summary>
        /// Map and build the roles list
        /// </summary>
        /// <param name="clientRoles"></param>
        /// <returns>Parsed list</returns>
        private static List<RoleInput> MapAndBuildRoleList(List<Role> clientRoles)
        {
            List<RoleInput> roles = new List<RoleInput>();

            if (clientRoles != null && clientRoles.Count > 0)
            {
                clientRoles.ForEach(item =>
                {
                    roles.Add(new RoleInput(
                        role: item.RoleType,
                        contributors: MapAndBuildContributorList(item.Contributors)));
                });
            }

            return roles;
        }

        /// <summary>
        /// Map and build the contributor list
        /// </summary>
        /// <param name="clientContributors"></param>
        /// <returns>Parsed contributor list</returns>
        private static List<ContributorInput> MapAndBuildContributorList(
            List<Contributor> clientContributors)
        {
            List<ContributorInput> contributors = new List<ContributorInput>();

            if (clientContributors != null && clientContributors.Count > 0)
            {
                clientContributors.ForEach(
                    item => { contributors.Add(new ContributorInput(id: item.Id)); });
            }

            return contributors;
        }

        /// <summary>
        /// Get contribution file from local or s3 bucket
        /// </summary>
        /// <param name="file"></param>
        /// <param name="folderPath"></param>
        /// <returns>Contribution file info</returns>
        private static FileInfo GetAndSaveContributionFile(ContributionFileType file,
                                                           string folderPath)
        {
            if (file.s3File != null)
            {
                return GetS3File(bucketName: file.s3File.BucketName,
                                 key: file.s3File.Key,
                                 awsKeyId: file.s3File.AWSKeyId,
                                 awsSecret: file.s3File.AWSSecret,
                                 storeFolderPath: folderPath);
            }

            string filePath = Path.GetFullPath(file.LocalFile.FilePath);
            return GetFileByPath(filePath);
        }

        /// <summary>
        /// Get file from path 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>FileInfo</returns>
        private static FileInfo GetFileByPath(string filePath)
        {
            if (File.Exists(filePath))
            {
                return new FileInfo(filePath);
            }

            return null;
        }

        /// <summary>
        /// Save file into Directory
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="file"></param>
        /// <returns>Saved file</returns>
        private static void SaveFileIntoDirectory(FileInfo file, string filePath)
        {
            string destinationFilePath = Path.Combine(filePath, file.Name);
            file.CopyTo(destinationFilePath, true);
        }

        /// <summary>
        /// Remove Directory
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>Removed Directory</returns>
        private static void RemoveDirectory(string filePath)
        {
            if (Directory.Exists(filePath))
            {
                Directory.Delete(filePath, true);
            }
        }

        /// <summary>
        /// Generate a new temp folder
        /// </summary>
        /// <returns></returns>
        private static string GetTemporaryDirectory()
        {
            string tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(tempDirectory);
            return tempDirectory;
        }

        /// <summary>
        /// Split the given file into multiple files
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="chunkSize"></param>
        /// <returns></returns>
        private static string SplitFile(string inputFile, int chunkSize)
        {
            Console.Out.WriteLine("Splitting file");

            string outputPath = GetTemporaryDirectory();
            Console.Out.WriteLine("Saving parts in: " + outputPath);
            int BUFFER_SIZE = chunkSize;
            byte[] buffer = new byte[BUFFER_SIZE];

            using (Stream input = File.OpenRead(inputFile))
            {
                int index = 0;
                while (input.Position < input.Length)
                {
                    using (Stream output = File.Create(outputPath + "\\part-" + index))
                    {
                        int remaining = chunkSize, bytesRead;
                        while (remaining > 0 && (bytesRead = input.Read(buffer, 0, Math.Min(remaining, BUFFER_SIZE))) > 0)
                        {
                            output.Write(buffer, 0, bytesRead);
                            remaining -= bytesRead;
                        }
                    }
                    index++;
                }
            }
            return outputPath;
        }


        /// <summary>
        /// Generate SHA3-256 for a given file path
        /// </summary>
        /// <param name="filePath">File path</param>
        /// <returns>Return string value with SHA3-256</returns>
        public static string getSHA256(string filePath)
        {
            return SHA256CheckSum(filePath);
        }

        /// <summary>
        /// Generate SHA3-256 for a given file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private static string SHA256CheckSum(string filePath)
        {
            using (SHA3256Managed sha3_256 = new SHA3256Managed())
            {
                using (FileStream fileStream = File.OpenRead(filePath))
                {
                    byte[] hashBytes = sha3_256.ComputeHash(fileStream);
                    StringBuilder hashBuilder = new StringBuilder(hashBytes.Length * 2);

                    foreach (byte b in hashBytes)
                    {
                        hashBuilder.Append(b.ToString("x2"));
                    }

                    Console.Out.WriteLine($"--- New sha256 generated: {hashBuilder.ToString()} for {filePath}");

                    return hashBuilder.ToString();
                }
            }
        }

        /// <summary>
        /// Method used to upload file into the session previously created
        /// </summary>
        /// <param name="filePath">String path where we have the file to be uploaded</param>
        public void UploadFile(string filePath)
        {
            Console.Out.WriteLine("File to upload: {0}", filePath);
            int tenMB = 6 * 1024 * 1024;
            long originalSize = new FileInfo(filePath).Length;
            string outputPath = SplitFile(filePath, tenMB);
            DirectoryInfo dir = new DirectoryInfo(outputPath);
            FileInfo[] Files = dir.GetFiles();

            foreach (FileInfo file in Files)
            {
                string fullPath = outputPath + "\\" + file.Name;
                int part = Int16.Parse(file.Name.Replace("part-", ""));
                Console.Out.WriteLine("Uploading part: {0}", part);
                UploadPart(String.Format("{0}-{1}/{2}", part * tenMB, part * tenMB + Math.Min(tenMB, file.Length), originalSize), SHA256CheckSum(fullPath), new UploadPartRequestContent(File.ReadAllBytes(fullPath)));
            }

        }

        /// <summary>
        /// Method to upload parts individually, use it if you dont want to use the UploadFile method
        /// </summary>
        /// <param name="contentRange">File content info</param>
        /// <param name="digest"></param>
        /// <param name="uploadPartRequestContent"></param>
        /// <returns></returns>
        public UploadPartResponseContent UploadPart(string contentRange = null, string digest = null, UploadPartRequestContent uploadPartRequestContent = null)
        {
            return API.UploadPart(this.UploadSessionID, this.APIKey, contentRange, digest, uploadPartRequestContent);
        }

        /// <summary>
        /// Method to be called once you already upload the files using UploadFile method or UploadPart individually
        /// </summary>
        /// <param name="commitUploadSessionRequestContent"></param>
        /// <returns></returns>
        public CommitUploadSessionResponseContent CommitUploadSession(CommitUploadSessionRequestContent commitUploadSessionRequestContent)
        {
            return API.CommitUploadSession(this.UploadSessionID, this.APIKey);
        }


        /// <summary>
        /// Method to be called once you already upload the files using UploadFile method or UploadPart individually
        /// <param name="contributionId">Parameter required</param>
        /// </summary>
        /// <returns></returns>
        public CommitUploadSessionResponseContent CommitUploadSession(string contributionId)
        {
            return this.CommitUploadSession(new CommitUploadSessionRequestContent(contributionId));
        }

        /// <summary>
        /// Getting the Upload Session information to know the status of the upload 
        /// </summary>
        /// <returns>Object with a summary of the upload process</returns>
        public GetUploadSessionResponseContent GetUploadSession()
        {
            return API.GetUploadSession(this.UploadSessionID, this.APIKey);
        }

        /// <summary>
        /// Getting the Upload Session information to know the status of the upload 
        /// </summary>
        /// <param name="limit">Quantity of records to be returned</param>
        /// <param name="offset">Pages to skip</param>
        /// <returns>Object with a list with all the uploaded parts</returns>
        public GetUploadSessionPartsResponseContent GetUploadListParts(int limit = 10, int offset = 0)
        {
            return API.GetUploadSessionParts(this.UploadSessionID, this.APIKey, limit, offset);
        }


    }
}
