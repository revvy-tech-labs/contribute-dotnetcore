/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing Property
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class PropertyTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for Property
        //private Property instance;

        public PropertyTests()
        {
            // TODO uncomment below to create an instance of Property
            //instance = new Property();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of Property
        /// </summary>
        [Fact]
        public void PropertyInstanceTest()
        {
            // TODO uncomment below to test "IsType" Property
            //Assert.IsType<Property>(instance);
        }


        /// <summary>
        /// Test the property 'RevvyId'
        /// </summary>
        [Fact]
        public void RevvyIdTest()
        {
            // TODO unit test for the property 'RevvyId'
        }
        /// <summary>
        /// Test the property 'Boundary'
        /// </summary>
        [Fact]
        public void BoundaryTest()
        {
            // TODO unit test for the property 'Boundary'
        }
        /// <summary>
        /// Test the property 'Location'
        /// </summary>
        [Fact]
        public void LocationTest()
        {
            // TODO unit test for the property 'Location'
        }
        /// <summary>
        /// Test the property 'Aliases'
        /// </summary>
        [Fact]
        public void AliasesTest()
        {
            // TODO unit test for the property 'Aliases'
        }

    }

}
