/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing CreateUploadSessionRequestContent
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class CreateUploadSessionRequestContentTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for CreateUploadSessionRequestContent
        //private CreateUploadSessionRequestContent instance;

        public CreateUploadSessionRequestContentTests()
        {
            // TODO uncomment below to create an instance of CreateUploadSessionRequestContent
            //instance = new CreateUploadSessionRequestContent();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of CreateUploadSessionRequestContent
        /// </summary>
        [Fact]
        public void CreateUploadSessionRequestContentInstanceTest()
        {
            // TODO uncomment below to test "IsType" CreateUploadSessionRequestContent
            //Assert.IsType<CreateUploadSessionRequestContent>(instance);
        }


        /// <summary>
        /// Test the property 'FileSize'
        /// </summary>
        [Fact]
        public void FileSizeTest()
        {
            // TODO unit test for the property 'FileSize'
        }
        /// <summary>
        /// Test the property 'Sha3256'
        /// </summary>
        [Fact]
        public void Sha3256Test()
        {
            // TODO unit test for the property 'Sha3256'
        }
        /// <summary>
        /// Test the property 'Contributions'
        /// </summary>
        [Fact]
        public void ContributionsTest()
        {
            // TODO unit test for the property 'Contributions'
        }

    }

}
