/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing ContributionRefData
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class ContributionRefDataTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for ContributionRefData
        //private ContributionRefData instance;

        public ContributionRefDataTests()
        {
            // TODO uncomment below to create an instance of ContributionRefData
            //instance = new ContributionRefData();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of ContributionRefData
        /// </summary>
        [Fact]
        public void ContributionRefDataInstanceTest()
        {
            // TODO uncomment below to test "IsType" ContributionRefData
            //Assert.IsType<ContributionRefData>(instance);
        }


        /// <summary>
        /// Test the property 'Page'
        /// </summary>
        [Fact]
        public void PageTest()
        {
            // TODO unit test for the property 'Page'
        }
        /// <summary>
        /// Test the property 'TotalPages'
        /// </summary>
        [Fact]
        public void TotalPagesTest()
        {
            // TODO unit test for the property 'TotalPages'
        }
        /// <summary>
        /// Test the property 'Contributions'
        /// </summary>
        [Fact]
        public void ContributionsTest()
        {
            // TODO unit test for the property 'Contributions'
        }

    }

}
