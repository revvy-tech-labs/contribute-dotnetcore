/*
 * ContributeService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing PropertieInfo
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class PropertieInfoTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for PropertieInfo
        //private PropertieInfo instance;

        public PropertieInfoTests()
        {
            // TODO uncomment below to create an instance of PropertieInfo
            //instance = new PropertieInfo();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of PropertieInfo
        /// </summary>
        [Fact]
        public void PropertieInfoInstanceTest()
        {
            // TODO uncomment below to test "IsType" PropertieInfo
            //Assert.IsType<PropertieInfo>(instance);
        }


        /// <summary>
        /// Test the property 'PropertyId'
        /// </summary>
        [Fact]
        public void PropertyIdTest()
        {
            // TODO unit test for the property 'PropertyId'
        }
        /// <summary>
        /// Test the property 'Address'
        /// </summary>
        [Fact]
        public void AddressTest()
        {
            // TODO unit test for the property 'Address'
        }
        /// <summary>
        /// Test the property 'Documents'
        /// </summary>
        [Fact]
        public void DocumentsTest()
        {
            // TODO unit test for the property 'Documents'
        }

    }

}
