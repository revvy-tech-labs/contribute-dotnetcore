/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing CostDetailDocument
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class CostDetailDocumentTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for CostDetailDocument
        //private CostDetailDocument instance;

        public CostDetailDocumentTests()
        {
            // TODO uncomment below to create an instance of CostDetailDocument
            //instance = new CostDetailDocument();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of CostDetailDocument
        /// </summary>
        [Fact]
        public void CostDetailDocumentInstanceTest()
        {
            // TODO uncomment below to test "IsType" CostDetailDocument
            //Assert.IsType<CostDetailDocument>(instance);
        }


        /// <summary>
        /// Test the property 'ReferenceId'
        /// </summary>
        [Fact]
        public void ReferenceIdTest()
        {
            // TODO unit test for the property 'ReferenceId'
        }
        /// <summary>
        /// Test the property 'Revvyid'
        /// </summary>
        [Fact]
        public void RevvyidTest()
        {
            // TODO unit test for the property 'Revvyid'
        }
        /// <summary>
        /// Test the property 'ContributionCostToken'
        /// </summary>
        [Fact]
        public void ContributionCostTokenTest()
        {
            // TODO unit test for the property 'ContributionCostToken'
        }

    }

}
