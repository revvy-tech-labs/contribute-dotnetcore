/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing GETContributorsResponse
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class GETContributorsResponseTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for GETContributorsResponse
        //private GETContributorsResponse instance;

        public GETContributorsResponseTests()
        {
            // TODO uncomment below to create an instance of GETContributorsResponse
            //instance = new GETContributorsResponse();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of GETContributorsResponse
        /// </summary>
        [Fact]
        public void GETContributorsResponseInstanceTest()
        {
            // TODO uncomment below to test "IsType" GETContributorsResponse
            //Assert.IsType<GETContributorsResponse>(instance);
        }


        /// <summary>
        /// Test the property 'Page'
        /// </summary>
        [Fact]
        public void PageTest()
        {
            // TODO unit test for the property 'Page'
        }
        /// <summary>
        /// Test the property 'TotalPages'
        /// </summary>
        [Fact]
        public void TotalPagesTest()
        {
            // TODO unit test for the property 'TotalPages'
        }
        /// <summary>
        /// Test the property 'Contributors'
        /// </summary>
        [Fact]
        public void ContributorsTest()
        {
            // TODO unit test for the property 'Contributors'
        }

    }

}
