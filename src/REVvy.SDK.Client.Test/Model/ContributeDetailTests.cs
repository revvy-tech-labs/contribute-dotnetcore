/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing ContributeDetail
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class ContributeDetailTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for ContributeDetail
        //private ContributeDetail instance;

        public ContributeDetailTests()
        {
            // TODO uncomment below to create an instance of ContributeDetail
            //instance = new ContributeDetail();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of ContributeDetail
        /// </summary>
        [Fact]
        public void ContributeDetailInstanceTest()
        {
            // TODO uncomment below to test "IsType" ContributeDetail
            //Assert.IsType<ContributeDetail>(instance);
        }


        /// <summary>
        /// Test the property 'Property'
        /// </summary>
        [Fact]
        public void PropertyTest()
        {
            // TODO unit test for the property 'Property'
        }
        /// <summary>
        /// Test the property 'Documents'
        /// </summary>
        [Fact]
        public void DocumentsTest()
        {
            // TODO unit test for the property 'Documents'
        }
        /// <summary>
        /// Test the property 'Roles'
        /// </summary>
        [Fact]
        public void RolesTest()
        {
            // TODO unit test for the property 'Roles'
        }

    }

}
