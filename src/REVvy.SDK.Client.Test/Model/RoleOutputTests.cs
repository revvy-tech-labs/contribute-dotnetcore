/*
 * UploadService
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * Generated by: https://github.com/openapitools/openapi-generator.git
 */


using Xunit;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Model;
using REVvy.SDK.Client.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace REVvy.SDK.Client.Test.Model
{
    /// <summary>
    ///  Class for testing RoleOutput
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by OpenAPI Generator (https://openapi-generator.tech).
    /// Please update the test case below to test the model.
    /// </remarks>
    public class RoleOutputTests : IDisposable
    {
        // TODO uncomment below to declare an instance variable for RoleOutput
        //private RoleOutput instance;

        public RoleOutputTests()
        {
            // TODO uncomment below to create an instance of RoleOutput
            //instance = new RoleOutput();
        }

        public void Dispose()
        {
            // Cleanup when everything is done.
        }

        /// <summary>
        /// Test an instance of RoleOutput
        /// </summary>
        [Fact]
        public void RoleOutputInstanceTest()
        {
            // TODO uncomment below to test "IsType" RoleOutput
            //Assert.IsType<RoleOutput>(instance);
        }


        /// <summary>
        /// Test the property 'Role'
        /// </summary>
        [Fact]
        public void RoleTest()
        {
            // TODO unit test for the property 'Role'
        }
        /// <summary>
        /// Test the property 'Contributors'
        /// </summary>
        [Fact]
        public void ContributorsTest()
        {
            // TODO unit test for the property 'Contributors'
        }

    }

}
