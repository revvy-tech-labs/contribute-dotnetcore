# REVvy.SDK.Client.Model.DocumentObj
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceId** | **string** |  | 
**Date** | **string** |  | 
**Type** | **string** |  | 
**Subtype** | **string** |  | 
**FileSize** | **long** |  | 
**Sha3256** | **string** |  | 
**PropertyID** | **string** |  | [optional] 
**Contributors** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

