# REVvy.SDK.Client.Model.Property
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RevvyId** | **string** |  | [optional] 
**Boundary** | [**List&lt;Coordinate&gt;**](Coordinate.md) |  | [optional] 
**Location** | [**Coordinate**](Coordinate.md) |  | [optional] 
**Aliases** | [**List&lt;Alias&gt;**](Alias.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

