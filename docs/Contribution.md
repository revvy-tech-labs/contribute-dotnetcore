# REVvy.SDK.Client.Model.Contribution
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceId** | **string** |  | 
**Property** | [**Property**](Property.md) |  | [optional] 
**PropertyId** | **string** |  | [optional] 
**Documents** | [**List&lt;DocumentObj&gt;**](DocumentObj.md) |  | 
**Contributors** | [**List&lt;ContributorInput&gt;**](ContributorInput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

