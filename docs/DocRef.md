# REVvy.SDK.Client.Model.DocRef
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | **DocStatus** |  | 
**ReferenceId** | **string** |  | 
**Revvyid** | **string** |  | 
**ContributionCostToken** | **double** |  | 
**TrustScore** | **int** |  | 
**Type** | **DocType** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

