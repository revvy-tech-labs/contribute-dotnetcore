# Org.OpenAPITools.Model.PropertieInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**PropertyId** | **string** |  | [optional] 
**Address** | [**Address**](Address.md) |  | [optional] 
**Documents** | [**List&lt;Document&gt;**](Document.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

