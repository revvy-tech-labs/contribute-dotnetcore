# REVvy.SDK.Client.Api.DefaultApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CancelUploadSession**](DefaultApi.md#canceluploadsession) | **DELETE** /contribute/upload-sessions/{uploadSessionID} | 
[**CommitUploadSession**](DefaultApi.md#commituploadsession) | **PATCH** /contribute/upload-sessions/{uploadSessionID} | 
[**CreateUploadSession**](DefaultApi.md#createuploadsession) | **POST** /contribute/upload-sessions/ | 
[**DeleteDocument**](DefaultApi.md#deletedocument) | **DELETE** /contributions/{contributionID}/documents/{documentID} | 
[**GetContributors**](DefaultApi.md#getcontributors) | **GET** /contributors/ | 
[**GetUploadSession**](DefaultApi.md#getuploadsession) | **GET** /contribute/upload-sessions/{uploadSessionID} | 
[**GetUploadSessionParts**](DefaultApi.md#getuploadsessionparts) | **GET** /contributions/upload-sessions/{uploadSessionID}/parts | 
[**GetUploadSessionRef**](DefaultApi.md#getuploadsessionref) | **GET** /contributions | 
[**Health**](DefaultApi.md#health) | **GET** /contribute/health | 
[**PostContributors**](DefaultApi.md#postcontributors) | **POST** /contributors/ | 
[**UploadPart**](DefaultApi.md#uploadpart) | **PUT** /contribute/upload-sessions/{uploadSessionID} | 
[**Version**](DefaultApi.md#version) | **GET** /contribute/version | 


<a name="canceluploadsession"></a>
# **CancelUploadSession**
> CancelUploadSessionResponseContent CancelUploadSession (string uploadSessionID, string xApiKey)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class CancelUploadSessionExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var uploadSessionID = uploadSessionID_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 

            try
            {
                CancelUploadSessionResponseContent result = apiInstance.CancelUploadSession(uploadSessionID, xApiKey);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.CancelUploadSession: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionID** | **string**|  | 
 **xApiKey** | **string**|  | 

### Return type

[**CancelUploadSessionResponseContent**](CancelUploadSessionResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **204** | CancelUploadSession 204 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="commituploadsession"></a>
# **CommitUploadSession**
> CommitUploadSessionResponseContent CommitUploadSession (string uploadSessionID, string xApiKey)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class CommitUploadSessionExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var uploadSessionID = uploadSessionID_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 

            try
            {
                CommitUploadSessionResponseContent result = apiInstance.CommitUploadSession(uploadSessionID, xApiKey);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.CommitUploadSession: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionID** | **string**|  | 
 **xApiKey** | **string**|  | 

### Return type

[**CommitUploadSessionResponseContent**](CommitUploadSessionResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | CommitUploadSession 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createuploadsession"></a>
# **CreateUploadSession**
> CreateUploadSessionResponseContent CreateUploadSession (string revvyKeyId, string xApiKey, CreateUploadSessionRequestContent createUploadSessionRequestContent)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class CreateUploadSessionExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var revvyKeyId = revvyKeyId_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 
            var createUploadSessionRequestContent = new CreateUploadSessionRequestContent(); // CreateUploadSessionRequestContent | 

            try
            {
                CreateUploadSessionResponseContent result = apiInstance.CreateUploadSession(revvyKeyId, xApiKey, createUploadSessionRequestContent);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.CreateUploadSession: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **revvyKeyId** | **string**|  | 
 **xApiKey** | **string**|  | 
 **createUploadSessionRequestContent** | [**CreateUploadSessionRequestContent**](CreateUploadSessionRequestContent.md)|  | 

### Return type

[**CreateUploadSessionResponseContent**](CreateUploadSessionResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | CreateUploadSession 200 response |  -  |
| **400** | BadRequestError 400 response |  -  |
| **403** | ForbiddenError 403 response |  -  |
| **409** | ConflictError 409 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletedocument"></a>
# **DeleteDocument**
> DeleteDocumentResponseContent DeleteDocument (string contributionID, string documentID, string revvyKeyId, string xApiKey)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class DeleteDocumentExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var contributionID = contributionID_example;  // string | 
            var documentID = documentID_example;  // string | 
            var revvyKeyId = revvyKeyId_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 

            try
            {
                DeleteDocumentResponseContent result = apiInstance.DeleteDocument(contributionID, documentID, revvyKeyId, xApiKey);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.DeleteDocument: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contributionID** | **string**|  | 
 **documentID** | **string**|  | 
 **revvyKeyId** | **string**|  | 
 **xApiKey** | **string**|  | 

### Return type

[**DeleteDocumentResponseContent**](DeleteDocumentResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | DeleteDocument 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getcontributors"></a>
# **GetContributors**
> GetContributorsResponseContent GetContributors (string revvyKeyId, string xApiKey, int? page = null, int? size = null, ContributorType? type = null, string q = null)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class GetContributorsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var revvyKeyId = revvyKeyId_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 
            var page = 56;  // int? |  (optional) 
            var size = 56;  // int? |  (optional) 
            var type = ;  // ContributorType? |  (optional) 
            var q = q_example;  // string |  (optional) 

            try
            {
                GetContributorsResponseContent result = apiInstance.GetContributors(revvyKeyId, xApiKey, page, size, type, q);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.GetContributors: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **revvyKeyId** | **string**|  | 
 **xApiKey** | **string**|  | 
 **page** | **int?**|  | [optional] 
 **size** | **int?**|  | [optional] 
 **type** | **ContributorType?**|  | [optional] 
 **q** | **string**|  | [optional] 

### Return type

[**GetContributorsResponseContent**](GetContributorsResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | GetContributors 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getuploadsession"></a>
# **GetUploadSession**
> GetUploadSessionResponseContent GetUploadSession (string uploadSessionID, string xApiKey)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class GetUploadSessionExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var uploadSessionID = uploadSessionID_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 

            try
            {
                GetUploadSessionResponseContent result = apiInstance.GetUploadSession(uploadSessionID, xApiKey);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.GetUploadSession: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionID** | **string**|  | 
 **xApiKey** | **string**|  | 

### Return type

[**GetUploadSessionResponseContent**](GetUploadSessionResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | GetUploadSession 200 response |  -  |
| **404** | NotFoundError 404 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getuploadsessionparts"></a>
# **GetUploadSessionParts**
> GetUploadSessionPartsResponseContent GetUploadSessionParts (string uploadSessionID, string xApiKey, int? page = null, int? size = null)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class GetUploadSessionPartsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var uploadSessionID = uploadSessionID_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 
            var page = 56;  // int? |  (optional) 
            var size = 56;  // int? |  (optional) 

            try
            {
                GetUploadSessionPartsResponseContent result = apiInstance.GetUploadSessionParts(uploadSessionID, xApiKey, page, size);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.GetUploadSessionParts: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionID** | **string**|  | 
 **xApiKey** | **string**|  | 
 **page** | **int?**|  | [optional] 
 **size** | **int?**|  | [optional] 

### Return type

[**GetUploadSessionPartsResponseContent**](GetUploadSessionPartsResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | GetUploadSessionParts 200 response |  -  |
| **404** | NotFoundError 404 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getuploadsessionref"></a>
# **GetUploadSessionRef**
> GetUploadSessionRefResponseContent GetUploadSessionRef (string xApiKey, string referenceId = null, string contributionId = null, int? page = null, int? size = null)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class GetUploadSessionRefExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var xApiKey = xApiKey_example;  // string | 
            var referenceId = referenceId_example;  // string |  (optional) 
            var contributionId = contributionId_example;  // string |  (optional) 
            var page = 56;  // int? |  (optional) 
            var size = 56;  // int? |  (optional) 

            try
            {
                GetUploadSessionRefResponseContent result = apiInstance.GetUploadSessionRef(xApiKey, referenceId, contributionId, page, size);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.GetUploadSessionRef: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **xApiKey** | **string**|  | 
 **referenceId** | **string**|  | [optional] 
 **contributionId** | **string**|  | [optional] 
 **page** | **int?**|  | [optional] 
 **size** | **int?**|  | [optional] 

### Return type

[**GetUploadSessionRefResponseContent**](GetUploadSessionRefResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | GetUploadSessionRef 200 response |  -  |
| **404** | InvalidInputError 404 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="health"></a>
# **Health**
> HealthResponseContent Health ()



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class HealthExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);

            try
            {
                HealthResponseContent result = apiInstance.Health();
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.Health: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**HealthResponseContent**](HealthResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Health 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="postcontributors"></a>
# **PostContributors**
> PostContributorsResponseContent PostContributors (string revvyKeyId, string xApiKey, PostContributorsRequestContent postContributorsRequestContent)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class PostContributorsExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var revvyKeyId = revvyKeyId_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 
            var postContributorsRequestContent = new PostContributorsRequestContent(); // PostContributorsRequestContent | 

            try
            {
                PostContributorsResponseContent result = apiInstance.PostContributors(revvyKeyId, xApiKey, postContributorsRequestContent);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.PostContributors: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **revvyKeyId** | **string**|  | 
 **xApiKey** | **string**|  | 
 **postContributorsRequestContent** | [**PostContributorsRequestContent**](PostContributorsRequestContent.md)|  | 

### Return type

[**PostContributorsResponseContent**](PostContributorsResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | PostContributors 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="uploadpart"></a>
# **UploadPart**
> UploadPartResponseContent UploadPart (string uploadSessionID, string xApiKey, string contentRange = null, string digest = null, UploadPartRequestContent uploadPartRequestContent = null)



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class UploadPartExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);
            var uploadSessionID = uploadSessionID_example;  // string | 
            var xApiKey = xApiKey_example;  // string | 
            var contentRange = contentRange_example;  // string |  (optional) 
            var digest = digest_example;  // string |  (optional) 
            var uploadPartRequestContent = new UploadPartRequestContent(); // UploadPartRequestContent |  (optional) 

            try
            {
                UploadPartResponseContent result = apiInstance.UploadPart(uploadSessionID, xApiKey, contentRange, digest, uploadPartRequestContent);
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.UploadPart: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uploadSessionID** | **string**|  | 
 **xApiKey** | **string**|  | 
 **contentRange** | **string**|  | [optional] 
 **digest** | **string**|  | [optional] 
 **uploadPartRequestContent** | [**UploadPartRequestContent**](UploadPartRequestContent.md)|  | [optional] 

### Return type

[**UploadPartResponseContent**](UploadPartResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | UploadPart 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="version"></a>
# **Version**
> VersionResponseContent Version ()



### Example
```csharp
using System.Collections.Generic;
using System.Diagnostics;
using REVvy.SDK.Client.Api;
using REVvy.SDK.Client.Client;
using REVvy.SDK.Client.Model;

namespace Example
{
    public class VersionExample
    {
        public static void Main()
        {
            Configuration config = new Configuration();
            config.BasePath = "http://localhost";
            // Configure API key authorization: smithy.api.httpApiKeyAuth
            config.AddApiKey("x-api-key", "YOUR_API_KEY");
            // Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
            // config.AddApiKeyPrefix("x-api-key", "Bearer");

            var apiInstance = new DefaultApi(config);

            try
            {
                VersionResponseContent result = apiInstance.Version();
                Debug.WriteLine(result);
            }
            catch (ApiException  e)
            {
                Debug.Print("Exception when calling DefaultApi.Version: " + e.Message );
                Debug.Print("Status Code: "+ e.ErrorCode);
                Debug.Print(e.StackTrace);
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**VersionResponseContent**](VersionResponseContent.md)

### Authorization

[smithy.api.httpApiKeyAuth](../README.md#smithy.api.httpApiKeyAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Version 200 response |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

