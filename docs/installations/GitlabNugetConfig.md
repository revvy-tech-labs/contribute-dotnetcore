# Steps to install Revvy SDK as Nuget Package from gitlab

To install a NuGet package from GitLab, you need to add GitLab as a package source and then install the package using the package manager.

## 1. Open the Nuget Package Manager

<br>

![Add Gitlab Nuget Step 1](docs/images/nuget-generator-step1.png)

## 2. Add new package source

<br>

![Add Gitlab Nuget Step 2](docs/images/nuget-generator-step2.png)

## 3. Add new source with the following values

<br>

![Add Gitlab Nuget Step 3](docs/images/nuget-generator-step3.png)

Source: [https://gitlab.com/api/v4/projects/38966857/packages/nuget/index.json](https://gitlab.com/api/v4/projects/38966857/packages/nuget/index.json)

## 4. Install the new Nuget Package

<br>

![Add Gitlab Nuget Step 4](docs/images/nuget-generator-step4.png)
