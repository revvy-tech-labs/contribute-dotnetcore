# REVvy.SDK.Client.Model.ContributionInput
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **ContributionType** |  | 
**ReferenceId** | **string** |  | 
**Property** | [**Property**](Property.md) |  | 
**Comments** | **string** |  | [optional] 
**Documents** | [**List&lt;DocumentInputObj&gt;**](DocumentInputObj.md) |  | 
**Roles** | [**List&lt;RoleInput&gt;**](RoleInput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

