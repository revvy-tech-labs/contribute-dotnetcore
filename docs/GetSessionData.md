# REVvy.SDK.Client.Model.GetSessionData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UploadSessionId** | **string** |  | 
**NumPartsProcessed** | **int** |  | 
**Committed** | **bool** |  | 
**SessionExpiresAt** | **string** |  | 
**TotalSize** | **long** |  | 
**PartSize** | **long** |  | 
**TotalParts** | **int** |  | 
**ContributionCostTokens** | **double** |  | 
**Sha3256** | **string** |  | 
**Contributions** | [**List&lt;ContributeDetail&gt;**](ContributeDetail.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

