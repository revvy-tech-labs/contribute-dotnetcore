# REVvy.SDK.Client.Model.ContributeDetail
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Property** | [**PropertyID**](PropertyID.md) |  | 
**Documents** | [**List&lt;DocumentInputObj&gt;**](DocumentInputObj.md) |  | 
**Roles** | [**List&lt;RoleOutput&gt;**](RoleOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

