# REVvy.SDK.Client.Model.GetUploadSessionPartsResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Parts** | [**List&lt;PartInfo&gt;**](PartInfo.md) |  | 
**NumPartsProcessed** | **int** |  | 
**TotalParts** | **int** |  | 
**SessionExpiresAt** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

