# REVvy.SDK.Client.Model.CommitUploadSessionRequestContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContributionId** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

