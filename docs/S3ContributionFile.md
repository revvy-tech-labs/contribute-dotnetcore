# REVvy.SDK.Client.Model.ContributionInput
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BucketName** | **string** |  | 
**Key** | **string** |  | 
**AWSKeyId** | **string** |  | 
**AWSSecret** | **string** |  |  

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

