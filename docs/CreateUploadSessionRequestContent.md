# REVvy.SDK.Client.Model.CreateUploadSessionRequestContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FileSize** | **int** |  | 
**Sha3256** | **string** |  | 
**Contributions** | [**List&lt;ContributionInput&gt;**](ContributionInput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

