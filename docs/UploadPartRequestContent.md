# REVvy.SDK.Client.Model.UploadPartRequestContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Body** | **byte[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

