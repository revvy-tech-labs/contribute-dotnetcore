# REVvy.SDK.Client.Model.ForbiddenErrorResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** |  | [default to "403"]
**Title** | **string** |  | [default to "Forbidden"]
**Description** | **string** |  | [default to "Returns an error if the operation is not allowed"]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

