# REVvy.SDK.Client.Model.ContributionRefData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Page** | **int** |  | 
**TotalPages** | **int** |  | 
**Contributions** | [**List&lt;ContributeDetailRef&gt;**](ContributeDetailRef.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

