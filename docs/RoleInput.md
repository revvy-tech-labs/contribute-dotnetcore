# REVvy.SDK.Client.Model.RoleInput
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Role** | **RoleType** |  | 
**Contributors** | [**List&lt;ContributorInput&gt;**](ContributorInput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

