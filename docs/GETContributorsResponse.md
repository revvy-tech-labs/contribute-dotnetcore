# REVvy.SDK.Client.Model.GETContributorsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Page** | **int** |  | 
**TotalPages** | **int** |  | 
**Contributors** | [**List&lt;ContributorOutput&gt;**](ContributorOutput.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

