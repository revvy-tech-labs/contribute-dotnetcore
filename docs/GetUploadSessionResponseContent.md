# REVvy.SDK.Client.Model.GetUploadSessionResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**GetUploadSessionOutputData**](GetUploadSessionOutputData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

