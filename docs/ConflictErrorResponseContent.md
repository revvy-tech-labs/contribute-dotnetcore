# REVvy.SDK.Client.Model.ConflictErrorResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** |  | [default to "409"]
**Title** | **string** |  | [default to "Conflict"]
**Description** | **string** |  | [default to "Returns an error if the contribution is a duplicate"]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

