# REVvy.SDK.Client.Model.ContributionCostDetail
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceId** | **string** |  | 
**Property** | [**Property**](Property.md) |  | 
**Documents** | [**List&lt;CostDetailDocument&gt;**](CostDetailDocument.md) |  | 
**Contributors** | [**List&lt;ContributorOutput&gt;**](ContributorOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

