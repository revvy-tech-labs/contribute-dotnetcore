# REVvy.SDK.Client.Model.UploadSessionData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Session** | [**SessionData**](SessionData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

