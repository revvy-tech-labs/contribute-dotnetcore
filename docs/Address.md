# Org.OpenAPITools.Model.Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**CountryCode** | **string** |  | 
**StreetName** | **string** |  | 
**Locality** | **string** |  | 
**Region** | **string** |  | 
**PostalCode** | **string** |  | 
**StreetNumber** | **string** |  | [optional] 
**StreetPreDirection** | **string** |  | [optional] 
**StreetSuffix** | **string** |  | [optional] 
**StreetPostDirection** | **string** |  | [optional] 
**UnitType** | **string** |  | [optional] 
**UnitNumber** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

