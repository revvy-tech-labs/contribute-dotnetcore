# REVvy.SDK.Client.Model.InvalidInputErrorResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **int** |  | [default to 400]
**Message** | **string** |  | [default to "Invalid Input"]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

