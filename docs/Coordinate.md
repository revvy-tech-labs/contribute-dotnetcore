# REVvy.SDK.Client.Model.Coordinate
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Lat** | **string** |  | 
**Lon** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

