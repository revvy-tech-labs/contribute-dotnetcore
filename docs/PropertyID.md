# REVvy.SDK.Client.Model.PropertyID
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RevvyId** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

