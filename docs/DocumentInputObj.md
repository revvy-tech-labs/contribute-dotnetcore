# REVvy.SDK.Client.Model.DocumentInputObj
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ReferenceId** | **string** |  | 
**Date** | **string** |  | 
**Type** | **DocType** |  | 
**Subtype** | **string** |  | 
**Comments** | **string** |  | [optional] 
**FileName** | **string** |  | 
**FileSize** | **long** |  | 
**Sha3256** | **string** |  | 
**PropertyId** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

