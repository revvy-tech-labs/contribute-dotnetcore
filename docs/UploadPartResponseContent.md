# REVvy.SDK.Client.Model.UploadPartResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**UploadPartOutputData**](UploadPartOutputData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

