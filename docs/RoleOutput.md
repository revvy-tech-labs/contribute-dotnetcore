# REVvy.SDK.Client.Model.RoleOutput
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Role** | **RoleType** |  | 
**Contributors** | [**List&lt;ContributorOutput&gt;**](ContributorOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

