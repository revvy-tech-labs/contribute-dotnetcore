# Org.OpenAPITools.Model.PropertiesStructure

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Properties** | [**List&lt;PropertieInfo&gt;**](PropertieInfo.md) |  | 
**ContainerFile** | [**ContainerFile**](ContainerFile.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

