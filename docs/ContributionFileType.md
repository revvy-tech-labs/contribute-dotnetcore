# REVvy.SDK.Client.Model.ContributionInput
## Properties for local File

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**LocalFile** | **string** |  | 

## Properties for AWS S3 File

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**S3File** | [**S3ContributionFile**](S3ContributionFile.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

