# REVvy.SDK.Client.Model.POSTContributorsResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Contributor** | [**ContributorOutput**](ContributorOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

