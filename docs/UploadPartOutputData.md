# REVvy.SDK.Client.Model.UploadPartOutputData
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**UploadSessionId** | **string** |  | 
**NumPartsProcessed** | **int** |  | 
**PartSize** | **long** |  | 
**SessionExpiresAt** | **string** |  | 
**TotalParts** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

