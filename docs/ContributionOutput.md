# REVvy.SDK.Client.Model.ContributionOutput
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **ContributionType** |  | 
**ReferenceId** | **string** |  | 
**Property** | [**Property**](Property.md) |  | 
**Comments** | **string** |  | [optional] 
**Documents** | [**List&lt;DocumentOutputObj&gt;**](DocumentOutputObj.md) |  | 
**Roles** | [**List&lt;RoleOutput&gt;**](RoleOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

