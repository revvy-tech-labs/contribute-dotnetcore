# REVvy.SDK.Client.Model.ContributeDetailRef
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | 
**Date** | **string** |  | 
**ReferenceId** | **string** |  | 
**ContributionCostToken** | **double** |  | 
**Status** | **ContributeStatus** |  | 
**Property** | [**Property**](Property.md) |  | 
**Documents** | [**List&lt;DocRef&gt;**](DocRef.md) |  | 
**Roles** | [**List&lt;RoleOutput&gt;**](RoleOutput.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

