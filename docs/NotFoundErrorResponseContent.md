# REVvy.SDK.Client.Model.NotFoundErrorResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **int** |  | [default to 404]
**Message** | **string** |  | [default to "Upload Session not Found"]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

