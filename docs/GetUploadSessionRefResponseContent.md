# REVvy.SDK.Client.Model.GetUploadSessionRefResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | [**ContributionRefData**](ContributionRefData.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

