# REVvy.SDK.Client.Model.BadRequestErrorResponseContent
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Code** | **string** |  | [default to "400"]
**Title** | **string** |  | [default to "Bad Request"]
**Description** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

