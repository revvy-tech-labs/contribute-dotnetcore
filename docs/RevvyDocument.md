# REVvy.SDK.Client.Model.DocumentInputObj
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**File** | [**ContributionFileType**](ContributionFileType.md) |  | 
**Date** | **string** |  | 
**Type** | **DocType** |  | 
**Subtype** | **string** |  |  [optional] 
**Comments** | **string** |  | [optional] 
**ReferenceId** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

