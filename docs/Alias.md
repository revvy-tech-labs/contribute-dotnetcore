# REVvy.SDK.Client.Model.Alias
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **AliasType** |  | 
**Values** | [**List&lt;Values&gt;**](Values.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

